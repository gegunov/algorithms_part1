package percolation;

import edu.princeton.cs.algs4.WeightedQuickUnionUF;


public class Percolation {

    private final WeightedQuickUnionUF weightedQuickUnionUF;
    private final WeightedQuickUnionUF additional;
    private final int size;
    private int numberOfOpenSites;
    private boolean[][] sites;

    public Percolation(int n) { // create n-by-n grid, with all sites blocked
        if (n <= 0) {
            throw new IllegalArgumentException();
        }
        weightedQuickUnionUF = new WeightedQuickUnionUF(n * n + 2);
        additional = new WeightedQuickUnionUF(n * n + 1);
        numberOfOpenSites = 0;
        sites = new boolean[n + 1][n + 1];
        size = n;

    }

    public void open(int row, int col) {  // open site (row, col) if it is not open already
        checkParameters(row, col);
        if (isOpen(row, col)) {
            return;
        }
        numberOfOpenSites++;
        sites[row - 1][col - 1] = true;

        if (row > 1 && isOpen(row - 1, col)) {
            connect(to1DArrInd(row, col), to1DArrInd(row - 1, col));
        }

        if (row < size && isOpen(row + 1, col)) {
            connect(to1DArrInd(row, col), to1DArrInd(row + 1, col));
        }
        if (row == 1) {
            connect(to1DArrInd(row, col), 0);
        } else if (row == size) {
            weightedQuickUnionUF.union(to1DArrInd(row, col), size * size);
        }
        if (col > 1 && isOpen(row, col - 1)) {
            connect(to1DArrInd(row, col), to1DArrInd(row, col - 1));
        }
        if (col < size && isOpen(row, col + 1)) {
            connect(to1DArrInd(row, col), to1DArrInd(row, col + 1));
        }
    }

    private void connect(int value, int value1) {
        weightedQuickUnionUF.union(value, value1);
        additional.union(value, value1);
    }

    public boolean isOpen(int row, int col) {  // is site (row, col) open?
        checkParameters(row, col);
        return sites[row - 1][col - 1];
    }

    public boolean isFull(int row, int col) { // is site (row, col) full?
        checkParameters(row, col);
        return isOpen(row, col) && additional.connected(to1DArrInd(row, col), 0);
    }

    public int numberOfOpenSites() {       // number of open sites
        return numberOfOpenSites;
    }

    public boolean percolates() {              // does the system percolate?
        return weightedQuickUnionUF.connected(0, size * size);
    }

    private void checkParameters(int row, int col) {
        if (row <= 0 || col <= 0 || row > size || col > size)
            throw new IllegalArgumentException();

    }

    private int to1DArrInd(int row, int col) {
        return (row - 1) * size + col;
    }

}