package percolation;

import edu.princeton.cs.algs4.StdOut;
import edu.princeton.cs.algs4.StdRandom;
import edu.princeton.cs.algs4.StdStats;

public class PercolationStats {


    private static final double THRESOLD = 1.96;
    private final int experimentsCount;
    private final double[] results;
    private double meanValue;
    private double stdDev;


    public PercolationStats(int n, int trials) {  // perform trials independent experiments on an n-by-n grid
        if (n <= 0 || trials <= 0) {
            throw new IllegalArgumentException();
        }

        experimentsCount = trials;
        results = new double[experimentsCount];
        for (int i = 0; i < experimentsCount; i++) {
            Percolation percolation = new Percolation(n);
            while (!percolation.percolates()) {
                int p = StdRandom.uniform(1, n + 1);
                int q = StdRandom.uniform(1, n + 1);
                if (!percolation.isOpen(p, q)) {
                    percolation.open(p, q);
                }
            }
            double fraction = (double) percolation.numberOfOpenSites() / (n * n);
            results[i] = fraction;

        }
    }

    public static void main(String[] args) {      // test client (described below)
        if (args.length != 2) {
            throw new IllegalArgumentException();
        }
        int size = Integer.parseInt(args[0]);
        int trials = Integer.parseInt(args[1]);
        PercolationStats percolationStats = new PercolationStats(size, trials);

        String confidence = percolationStats.confidenceLo() + ", " + percolationStats.confidenceHi();
        StdOut.println("mean                    = " + percolationStats.mean());
        StdOut.println("stddev                  = " + percolationStats.stddev());
        StdOut.println("95% confidence interval = " + confidence);
    }

    public double mean() {                      // sample mean of resources.percolation threshold
        if (meanValue == 0.0) {
            meanValue = StdStats.mean(results);
        }
        return meanValue;
    }

    public double stddev() {                   // sample standard deviation of resources.percolation threshold
        if (stdDev == 0.0) {
            stdDev = StdStats.stddev(results);
        }
        return stdDev;
    }

    public double confidenceLo() {              // low  endpoint of 95% confidence interval
        return mean() - ((THRESOLD * stddev()) / Math.sqrt(experimentsCount));
    }

    public double confidenceHi() {       // high endpoint of 95% confidence interval
        return mean() + ((THRESOLD * stddev()) / Math.sqrt(experimentsCount));
    }
}
